Front End Testing of a Node App
=============
### Overview

A Todo MEAN App Integrated with End to End Automated Test

The MEAN App was cloned from https://github.com/arvindr21/MEAN-Todo-App 
and integrated with Automated test using [nightwatchjs](http://nightwatchjs.org/). 
This project also utilizes [qualitywatcher](https://github.com/QualityWorksCG/qualitywatcher) 
so the test results can be viewed on the [qualitywatcher.io](http://qualitywatcher.io/) dashboard.

### Install & Run Application

* Download/clone the repo
* Run `npm install`
* Run `gulp` and navigate to `http://localhost:3000` to view the app

### Automation

* Run Test
    * `npm test`
* Run Test and see reports on [qualitywatcher.io](http://qualitywatcher.io/) dashboard
    * Repos have to belong to bitbucket... **[contact us to use github]()**
    * See [qualitywatcher](https://github.com/QualityWorksCG/qualitywatcher) node mdoule for setup